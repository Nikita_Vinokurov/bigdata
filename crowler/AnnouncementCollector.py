import os
from selenium.webdriver.firefox.firefox_binary import FirefoxBinary
from selenium import webdriver
from selenium.common.exceptions import ElementClickInterceptedException
import socks
import socket
import time
import pandas as pd
import numpy as np


#here enter your path to tor browser file
binary = '/Applications/Tor Browser.app/Contents/MacOS/firefox'
if os.path.exists(binary) is False:
    raise ValueError("The binary path to Tor firefox does not exist.")
firefox_binary = FirefoxBinary(binary)
browser = None
df = pd.read_csv('refs.csv')
refs = np.array(df['ref'])


def get_browser(binary=None):
    global browser
    # only one instance of a browser opens, remove global for multiple instances
    if not browser:
        browser = webdriver.Firefox(firefox_binary=binary)
    return browser


def get_page(url):
    socks.set_default_proxy(socks.SOCKS5, "localhost", 9150)
    socket.socket = socks.socksocket
    browser.get(url)
    time.sleep(10)


def get_announcement_info(announcement, browser):
    ref = announcement.find_element_by_tag_name("a").get_attribute("href")
    if ref not in refs:
        data = pd.DataFrame([ref], columns=['ref'])
        data.to_csv(r'refs.csv', mode='a', header=False)


if __name__ == "__main__":
    browser = get_browser(binary=firefox_binary)
    get_page('https://realty.yandex.ru/sankt-peterburg_i_leningradskaya_oblast/?from=yandex_direct&utm_source=yandex_di'
             'rect&utm_medium=cpc&utm_campaign=spb_brand_poisk&utm_content=5433362319&utm_term=яндекс%20недвижимость%20'
             'спб&_openstat=ZGlyZWN0LnlhbmRleC5ydTszMzI4MzAzODs1NDMzMzYyMzE5O3lhbmRleC5ydTpwcmVtaXVt&yclid=101255725165'
             '347716')
    print("Type some button")
    input()
    elements = browser.find_elements_by_tag_name('button')
    button = None
    for element in elements:
        if element.text.startswith('Показать'):
            button = element
    while True:
        while True:
            try:
                button.click()
                break
            except ElementClickInterceptedException:
                print("Type some button")
                input()
        time.sleep(15)
        announcements = browser.find_elements_by_xpath("//li[@class='OffersSerpItem OffersSerpItem_view_desktop OffersSer"
                                                       "pItem_format_full OffersSerp__list-item OffersSerp__list-item_t"
                                                       "ype_offer']")
        refs = browser.find_elements_by_xpath("//a[@class='Link Link_js_inited Link_size_m Link_theme_islands SerpItemLink"
                                              " OffersSerpItem__link OffersSerpItem__left']")
        for announcement in announcements:
            get_announcement_info(announcement, browser)
            elements = browser.find_elements_by_tag_name('button')
        button = None
        for element in elements:
            if 'Следующая' in element.text:
                button = element
        if button is None:
            break
