import os
from selenium.webdriver.firefox.firefox_binary import FirefoxBinary
from selenium import webdriver
from selenium.common.exceptions import ElementClickInterceptedException
import socks
import socket
import time
import pandas as pd
import numpy as np


#here enter your path to tor browser file
binary = '/Applications/Tor Browser.app/Contents/MacOS/firefox'
if os.path.exists(binary) is False:
    raise ValueError("The binary path to Tor firefox does not exist.")
firefox_binary = FirefoxBinary(binary)
browser = None


def get_browser(binary=None):
    global browser
    # only one instance of a browser opens, remove global for multiple instances
    if not browser:
        browser = webdriver.Firefox(firefox_binary=binary)
    return browser


def get_page(url):
    socks.set_default_proxy(socks.SOCKS5, "localhost", 9150)
    socket.socket = socks.socksocket
    browser.get(url)
    time.sleep(10)


if __name__ == "__main__":
    browser = get_browser(binary=firefox_binary)
    refs = pd.read_csv('refs.csv')
    for ref in refs['ref']:
        get_page(ref)
        address = browser.find_element_by_xpath(
            "//div[@class='OfferHeader__address']").text
        price = browser.find_element_by_xpath(
            "//span[@class='price']").text
        elements = browser.find_elements_by_xpath(
            "//div[@class='ColumnsList__item']")
        area = ""
        rooms = ""
        floor = ""
        for element in elements:
            if str(element.text).find("площадь") != -1:
                area = element.text
            if str(element.text).find("комн") != -1:
                rooms = element.text
            if str(element.text).find("этаж") != -1:
                floor = element.text
        price = price.replace(" ", "")
        price = price.replace("₽", "")
        price = int(price.replace(" ", ""))
        area = float(area.split(" ")[0].replace(",", "."))
        rooms = int(rooms.split(" ")[0])
        floors = int(floor.split(" ")[len(floor.split(" ")) - 1])
        floor = int(floor.split(" ")[0])
        new_row = pd.DataFrame([[price, address, area, rooms, floor, floors]],
                               columns=["цена", "адрес", "площадь", "комнаты", "этаж", "этажность"])
        new_row.to_csv('data.csv', mode='a', header=False, sep=";")

