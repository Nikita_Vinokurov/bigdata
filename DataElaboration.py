import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import math
import seaborn as sns
import re
from pyspark import SparkContext, SparkConf
from pyspark.sql import SparkSession


def list_to_string(lst):
    s = ""
    for element in lst:
        s += str(element) + "  ||  "
    return s


def get_distance(x1, y1, x2, y2):
    y1 = float(y1)
    y2 = float(y2)
    x1 = float(x1)
    x2 = float(x2)
    return math.sqrt(-14602429551 * (x1 - x2) ** 2 + 301547823132 * (y1 - y2) ** 2)


def get_area(s):
    s = re.findall(r'Площадь=[0-9]{1,}', s)
    s = re.findall(r'[0-9]{1,}', s[0])
    return float(s[0])


def get_sqm_price(x, y):
    return x / y


def get_year_month_part_of_month(date):
    year_month = date[:8]
    day = int(date[8:10])
    part = 1
    if 5 <= day < 10:
        part = 2
    if 10 <= day < 15:
        part = 3
    if 15 <= day < 20:
        part = 4
    if 20 <= day < 25:
        part = 5
    if 25 <= day <= 31:
        part = 6
    year_month = year_month + str(part)
    return year_month


def get_modified_data(street_name_english, app_type):
    file = pd.read_csv(street_name_english + '.csv')
    file = file[file['Тип объявления'] == app_type]
    prices = file[['Название', 'Адрес', 'Дата', 'Цена', 'Тип объявления', 'Доп.параметры']].copy()
    prices = prices[prices['Цена'] != 'Цена']
    prices = prices[prices['Доп.параметры'].str.contains(r'Площадь=[0-9]{1,}', case=False, na=False)]
    prices['Год-месяц'] = prices.apply(lambda x: get_year_month(x['Дата']), axis=1)
    prices['Год-месяц-часть месяца'] = \
        prices.apply(lambda x: get_year_month_part_of_month(x['Дата']), axis=1)
    prices['Числовая цена'] = prices.apply(lambda x: to_num(x['Цена']), axis=1)
    prices['Площадь'] = prices.apply(lambda x: get_area(x['Доп.параметры']), axis=1)
    prices = prices[prices['Площадь'] > 0]
    prices['Цена кв.м.'] = prices.apply(lambda x: get_sqm_price(x['Числовая цена'], x['Площадь']), axis=1)
    return prices


def get_price_3sigma_limits(street_name_english, app_type):
    prices = get_modified_data(street_name_english, app_type)
    arr = np.array(prices['Числовая цена'])
    return arr.mean() - 3 * np.sqrt(arr.var()), arr.mean() + 3 * np.sqrt(arr.var())


def collect_data_for_street(street_name_russian, street_name_english):
    for i in range(1, 9):
        print("started " + str(i))
        filename = 'excel-13105-' + str(i) + '.xlsx'
        dataset1 = pd.read_excel(filename)
        mask = dataset1.Адрес.str.contains(street_name_russian, case=False, na=False)
        data = dataset1[mask]
        df = data[['Название', 'Цена', 'Дата', 'Адрес', 'Тип объявления', 'Описание', 'Доп.параметры']]
        df.to_csv(street_name_english + '.csv', mode='a')
        print("finished " + str(i))


def collect_data_for_metro(street_name_russian, street_name_english):
    for i in range(1, 9):
        print("started " + str(i))
        filename = 'excel-13105-' + str(i) + '.xlsx'
        dataset1 = pd.read_excel(filename)
        mask = dataset1['Метро/Район'].str.contains(street_name_russian, case=False, na=False)
        data = dataset1[mask]
        df = data[['Название', 'Цена', 'Дата', 'Адрес', 'Тип объявления', 'Описание', 'Доп.параметры']]
        df.to_csv(street_name_english + '.csv', mode='a')
        print("finished " + str(i))


def collect_data_for_district(x_c, y_c, name_english, distance_km):
    for i in range(1, 9):
        print("started " + str(i))
        filename = 'excel-13105-' + str(i) + '.xlsx'
        dataset1 = pd.read_excel(filename)
        dataset1['Расстояние'] = dataset1.apply(lambda x: get_distance(x_c, y_c, x['lat'], x['lng']), axis=1)
        data = dataset1[dataset1['Расстояние'] < 1.3]
        df = data[['Название', 'Цена', 'Дата', 'Адрес', 'Тип объявления', 'Описание', 'Доп.параметры']]
        df.to_csv(name_english + '.csv', mode='a')
        print("finished " + str(i))


def plot_average_month_price_sell(street_name_russian,
                                  street_name_english,
                                  app_type,
                                  apartments=False,
                                  rooms=None,
                                  house_type=None):
    """Parameter apartments is boolean, shows whether user wants to look for only apartments or not.
    Parameter rooms is int or None or str with value 'Студия', shows number of rooms in apartment.
    Parameter house_type if not None has 2 values: 'Новостройка' and 'Вторичка'."""
    prices = get_modified_data(street_name_english, app_type)
    if apartments:
        prices = prices[prices['Доп.параметры'].str.contains('Площадь кухни=', case=False, na=False)]
        if rooms is not None:
            prices = prices[prices['Доп.параметры'].str
                .contains('Количество комнат=' + str(rooms), case=False, na=False)]
        if house_type is not None:
            prices = prices[prices['Доп.параметры'].str
                .contains('Вид объекта=' + str(house_type), case=False, na=False)]
    limits = get_price_3sigma_limits(street_name_english, app_type)
    prices = prices[prices['Числовая цена'] > limits[0]]
    prices = prices[prices['Числовая цена'] <= limits[1]]
    #    prices = prices[['Год-месяц', 'Числовая цена']]
    average_prices = prices.groupby('Год-месяц').mean()
    return average_prices


def plot_month_quantities(street_name_russian,
                          street_name_english,
                          app_type,
                          apartments=False,
                          rooms=None,
                          house_type=None):
    """Parameter apartments is boolean, shows whether user wants to look for only apartments or not.
    Parameter rooms is int or None or str with value 'Студия', shows number of rooms in apartment.
    Parameter house_type if not None has 2 values: 'Новостройка' and 'Вторичка'."""
    prices = get_modified_data(street_name_english, app_type)
    if apartments:
        prices = prices[prices['Доп.параметры'].str.contains('Площадь кухни=', case=False, na=False)]
        if rooms is not None:
            prices = prices[prices['Доп.параметры'].str
                .contains('Количество комнат=' + str(rooms), case=False, na=False)]
        if house_type is not None:
            prices = prices[prices['Доп.параметры'].str
                .contains('Вид объекта=' + str(house_type), case=False, na=False)]
    limits = get_price_3sigma_limits(street_name_english, app_type)
    prices = prices[prices['Числовая цена'] > limits[0]]
    prices = prices[prices['Числовая цена'] <= limits[1]]
    quantities = prices.groupby('Год-месяц').count()
    return quantities


def get_year_month(date):
    return date[:7]


def to_num(x):
    return int(x)


def plot_graphs_for_average_price_and_quantity(street_name_russian, street_name_english, app_type, rooms):
    average_prices_new = plot_average_month_price_sell(street_name_russian,
                                                       street_name_english,
                                                       app_type,
                                                       True, rooms, 'Новостройка')
    average_prices_vtor = plot_average_month_price_sell(street_name_russian,
                                                        street_name_english,
                                                        app_type,
                                                        True, rooms, 'Вторичка')
    quantity_new = plot_month_quantities(street_name_russian,
                                         street_name_english,
                                         app_type,
                                         True, rooms, 'Новостройка')
    quantity_vtor = plot_month_quantities(street_name_russian,
                                          street_name_english,
                                          app_type,
                                          True, rooms, 'Вторичка')
    fig = plt.figure(figsize=(12, 6))
    fig.suptitle(street_name_russian + ', ' + str(rooms) + '-комнатные квартиры', va='top', fontsize=16)
    plt.subplot(2, 2, 1)
    plt.grid(True)
    print(average_prices_new.keys())
    plt.plot(average_prices_new)
    plt.xlim('2019-10', '2018-10')
    fig.autofmt_xdate()
    plt.title('новостройки, среднемесяная цена', ha='right')
    plt.subplot(2, 2, 3)
    plt.grid(True)
    plt.plot(quantity_new)
    plt.xlim('2019-10', '2018-10')
    fig.autofmt_xdate()
    plt.title('новостройки, количество в месяц', ha='right')
    plt.subplot(2, 2, 2)
    plt.grid(True)
    plt.plot(average_prices_vtor)
    plt.xlim('2019-10', '2018-10')
    fig.autofmt_xdate()
    plt.title('вторичка, среднемесяная цена', ha='left')
    plt.subplot(2, 2, 4)
    plt.grid(True)
    plt.plot(quantity_vtor)
    plt.xlim('2019-10', '2018-10')
    fig.autofmt_xdate()
    plt.title('вторичка, количество в месяц', ha='left')
    plt.show()


def get_perc(x):
    return x['std'] / x['mean'] * 100


def plot(prices, rooms, complex, complex_english):
    prices = prices[prices['Доп.параметры'].str.contains('Площадь кухни=', case=False, na=False)]
    prices = prices[prices['Доп.параметры'].str.contains('Количество комнат=' + str(rooms), case=False, na=False)]
    prices = prices[prices['Доп.параметры'].str.contains('Вид объекта=Новостройка', case=False, na=False)]
    prices = prices[prices['Доп.параметры'].str.contains('Название ЖК=' + complex, case=False, na=False)]
    res = (prices.assign(idx=prices.groupby('Год-месяц-часть месяца').cumcount())
           .pivot_table(index='idx', columns='Год-месяц-часть месяца',
                        values='Цена кв.м.', aggfunc='sum'))
    fig = plt.figure(figsize=(12, 12))
    plt.subplot(2, 1, 1)
    sns.boxplot(data=res, width=0.2)
    plt.plot(prices[['Год-месяц-часть месяца', 'Цена кв.м.']].groupby('Год-месяц-часть месяца').mean())
    if str(rooms) != 'Студия':
        plt.title('Среднемесяная цена за квадратный метр на ' + str(rooms) + '-к кв. в жк ' + complex)
    else:
        plt.title('Среднемесяная цена за квадратный метр на студии в жк ' + complex)
    plt.ylabel('Среднемесяная цена за квадратный метр')
    plt.subplot(2, 1, 2)
    plt.plot(prices.groupby('Год-месяц-часть месяца').count())
    if str(rooms) != 'Студия':
        plt.title('Количество объявлений на ' + str(rooms) + '-к кв. в жк ' + complex)
    else:
        plt.title('Количество объявлений на студии в жк ' + complex)
    plt.ylabel('количество обявлений')
    fig.autofmt_xdate()
    plt.savefig('/Users/user/PycharmProjects/ImmobiliarServ/parnas/complexes_for_parts_of_months/'
                + complex_english + '/' + str(rooms) + '.png', format='png', dpi=100)
    plt.clf()


def plot_announcement_quantities(prices, s=False, r1=False, r2=False, r3=False):
    fig = plt.figure()
    plt.title('Quantities of announcements')
    plt.xlabel('quantity')
    plt.ylabel('part of month')
    if s:
        stud = prices[prices['Доп.параметры'].str.contains('Количество комнат=Студия', case=False, na=False)]
        stud = stud.groupby('Год-месяц-часть месяца').count()
        plt.plot(stud, c='g')
        plt.plot([], [], c='g', label='Studio apartments')
    if r1:
        room1 = prices[prices['Доп.параметры'].str.contains('Количество комнат=1', case=False, na=False)]
        plt.plot(room1.groupby('Год-месяц-часть месяца').count(), c='r')
        plt.plot([], [], c='r', label='1-room apartment')
    if r2:
        room2 = prices[prices['Доп.параметры'].str.contains('Количество комнат=2', case=False, na=False)]
        plt.plot(room2.groupby('Год-месяц-часть месяца').count(), c='b')
        plt.plot([], [], c='b', label='2-room apartment')
    if r3:
        room3 = prices[prices['Доп.параметры'].str.contains('Количество комнат=3', case=False, na=False)]
        plt.plot(room3.groupby('Год-месяц-часть месяца').count(), c='y')
        plt.plot([], [], c='y', label='3-room apartment')
    fig.autofmt_xdate()
    plt.legend(loc=1)
    return fig


conf = SparkConf().setAppName('appName').setMaster('local')
sc = SparkContext(conf=conf)
spark = SparkSession(sc)


def plot_average_month_price_with_pyspark(path_to_csv):
    df = spark.read.format("csv").option("header", "true").load(path_to_csv)
    df = df.withColumn("Цена", df["Цена"].cast("float"))
    df = df.select("Год-месяц-часть месяца", "Цена") \
        .groupBy('Год-месяц-часть месяца') \
        .avg("Цена")
    df.show()
    fig = plt.figure()
    df.toPandas().plot(x="Год-месяц-часть месяца")
    plt.ylabel('Среднемесяная цена за квадратный метр')
    fig.autofmt_xdate()
    plt.show()


def plot_announcement_number_with_pyspark(path_to_csv):
    df = spark.read.format("csv").option("header", "true").load(path_to_csv)
    df = df.select("Год-месяц-часть месяца", "Цена") \
        .groupBy('Год-месяц-часть месяца') \
        .count()
    df.show()
    fig = plt.figure()
    df.toPandas().plot(x="Год-месяц-часть месяца")
    plt.ylabel('Количество объявлений')
    fig.autofmt_xdate()
    plt.show()
