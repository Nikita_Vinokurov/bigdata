import pandas as pd
import os
import matplotlib.pyplot as plt
import seaborn as sns
import re


def get_year_month_part_of_month(date):
    year_month = date[:8]
    day = int(date[8:10])
    part = 1
    if 5 <= day < 10:
        part = 2
    if 10 <= day < 15:
        part = 3
    if 15 <= day < 20:
        part = 4
    if 20 <= day < 25:
        part = 5
    if 25 <= day <= 31:
        part = 6
    year_month = year_month + str(part)
    return year_month


def get_sqm_price(x, y):
    return x / y


def to_num(x):
    return int(x)


def get_area(s):
    s = re.findall(r'Площадь=[0-9]{1,}', s)
    s = re.findall(r'[0-9]{1,}', s[0])
    return float(s[0])


class Complex:
    def __init__(self, complex, working_directory="", path_to_locate_graphs="ComplexesGraphs/"):
        self.complex = complex
        self.temp_csv = self.complex + ".csv"
        self.folder_for_graphs = path_to_locate_graphs
        os.mkdir(path_to_locate_graphs + self.complex)

    def form_temporal_csv(self, files):
        for file in files:
            df = pd.read_csv(file)
            mask = df['Доп.параметры'].str.contains(self.complex, case=False, na=False)
            df = df[mask]
            df.to_csv(self.temp_csv, mode='a')

    def delete_csv(self):
        os.remove(self.temp_csv)

    def plot_graph(self, rooms):
        prices = pd.read_csv(self.complex + ".csv")
        prices = prices[prices['Цена'] != 'Цена']
        prices['Числовая цена'] = prices.apply(lambda x: to_num(x['Цена']), axis=1)
        prices['Год-месяц-часть месяца'] = \
            prices.apply(lambda x: get_year_month_part_of_month(x['Дата']), axis=1)
        prices['Площадь'] = prices.apply(lambda x: get_area(x['Доп.параметры']), axis=1)
        prices = prices[prices['Площадь'] > 0]
        prices['Цена кв.м.'] = prices.apply(lambda x: get_sqm_price(x['Числовая цена'], x['Площадь']), axis=1)
        prices = prices[prices['Доп.параметры'].str.contains('Количество комнат=' + str(rooms), case=False, na=False)]
        res = (prices.assign(idx=prices.groupby('Год-месяц-часть месяца').cumcount())
               .pivot_table(index='idx', columns='Год-месяц-часть месяца',
                            values='Цена кв.м.', aggfunc='sum'))
        fig = plt.figure(figsize=(12, 12))
        plt.subplot(2, 1, 1)
        sns.boxplot(data=res, width=0.2)
        plt.plot(prices[['Год-месяц-часть месяца', 'Цена кв.м.']].groupby('Год-месяц-часть месяца').mean())
        if str(rooms) != 'Студия':
             plt.title('Average month price of sq. mt. for ' + str(rooms) + '-roomed apps. in \"' + self.complex + "\"")
        else:
             plt.title('Average month price of sq. mt. for studios in ' + self.complex)
        plt.ylabel('Average month price of sq. mt.')
        plt.subplot(2, 1, 2)
        plt.plot(prices.groupby('Год-месяц-часть месяца')['Площадь'].count())
        if str(rooms) != 'Студия':
            plt.title('Number of announcements for ' + str(rooms) + '-roomed apps. in \"' + self.complex + "\"")
        else:
            plt.title('Number of announcements for studios in ' + self.complex)
        plt.ylabel('number of announcements')
        fig.autofmt_xdate()
        plt.savefig(self.folder_for_graphs + self.complex + "/" + rooms + "/graph.png", format='png', dpi=100)
        plt.clf()
        plt.close()

    def plot_graphs(self):
        rooms = ['Студия', '1', '2', '3']
        for room in rooms:
            os.mkdir(self.folder_for_graphs + self.complex + "/" + room)
            self.plot_graph(room)


file = open("NewHouses/complexes.txt")
files = []
for i in range(1, 9):
    filename = 'excel-13105-' + str(i) + '.csv'
    files.append(filename)
complexes = file.readlines()
os.chdir("NewHouses")
for i in range(len(complexes)):
    complexes[i] = complexes[i][:len(complexes[i]) - 1]
for complex1 in complexes:
    if complex1.startswith("!") or complex1.startswith("*"):
        continue
    compl = Complex(complex1)
    compl.form_temporal_csv(files)
    try:
        compl.plot_graphs()
    except Exception:
        compl.delete_csv()
        failed = open("failed.txt", "w")
        failed.write(complex1)
        failed.close()
        continue
    compl.delete_csv()

